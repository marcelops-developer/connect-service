package dev.connect.web.indicator;

import dev.connect.web.indicator.client.service.IndicatorService;
import org.eclipse.microprofile.rest.client.inject.RestClient;

import javax.inject.Inject;
import javax.validation.constraints.NotNull;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;

@Path("/api/world-bank")
public class IndicatorResource {

    @Inject
    @RestClient
    IndicatorService indicatorService;

    @GET
    @Path("/{code}")
    @Produces(MediaType.APPLICATION_JSON)
    public Object getIndicators(
            @NotNull @PathParam("code") String code,
            @QueryParam("page") String page,
            @QueryParam("per_page") String per_page
    ) {
        return indicatorService.getIndicators(code, page, per_page, "json");
    }
}