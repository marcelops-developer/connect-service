package dev.connect.web.indicator.client.service;
import org.eclipse.microprofile.rest.client.inject.RegisterRestClient;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;

@Path("/v2")
@RegisterRestClient
public interface IndicatorService {
    @GET
    @Path("/country/{code}/indicator/SI.POV.DDAY")
    @Produces(MediaType.APPLICATION_JSON)
    public Object getIndicators(
            @PathParam("code") String name,
            @QueryParam("page") String page,
            @QueryParam("per_page") String per_page,
            @QueryParam("format") String format
    );
}
