package dev.connect.web.indicator;

import io.quarkus.test.junit.QuarkusTest;
import org.junit.jupiter.api.Test;

import static io.restassured.RestAssured.given;
import static org.hamcrest.CoreMatchers.is;

@QuarkusTest
public class IndicatorResourceTest {

    @Test
    public void testGetIndicatorEndpoint() {
        given()
          .when().get("/api/world-bank/BRA")
          .then()
             .statusCode(200);
    }

}